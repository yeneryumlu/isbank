#### set env variable

RHSSO_REGISTRY_ISBANK="scorenexus.isbank:9443/redhat-sso/redhatsso:0.0.1"  
MASTER_URL="https://redhat-sso.kube.uatisbankk"  
OCP4_ADMIN_USER="admin"  
OCP4_ADMIN_PWD="password"  
RHSSO_PROJECT="redhat-sso"  
APPLICATION_NAME="sso"  


### Step 1 - Install the single sign-on resources on OpenShift.

> git clone https://Jennix2016@bitbucket.org/Jennix2016/isbank.git

>cat templates/sso74-x509-https.json   
>   - check the parameters

### Step 2 - Verify that the single sign-on container image is available inside the registry

>podman search ${RHSSO_REGISTRY_ISBANK}

### Step 3 - Create secret to hold DB credentials

//TODO  
DB_USERNAME	redhatsso  
DB_PASSWORD	asdasd  
DB_URL	(DESCRIPTION= (ADDRESS= (PROTOCOL=TCP) (HOST=i1cnsyora1.intisbank) (PORT=1521)) (CONNECT_DATA= (SERVER=dedicated) (SERVICE_NAME=SRV_REDHATSSO2_ICNSY)))
 
 oc create secret generic ${APPLICATION_NAME}-db-connection --from-literal=username=<user_name> --from-literal=password=<password> 

### Step 3 - Create the application from the template

#### Log into OCP and switch to your project

>oc login -u ${OCP4_ADMIN_USER} -p OCP4_ADMIN_PWD ${MASTER_URL}

>oc project ${RHSSO_PROJECT}

#### Run the following command

>oc process -f isbank-sso74-x509-https.json -p APPLICATION_NAME=sso -p SSO_ADMIN_USERNAME=7V2Kklmf -p SSO_ADMIN_PASSWORD=asdasd | oc create -f -
